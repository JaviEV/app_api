package com.example.defapp;

import android.content.Context;
import android.graphics.Movie;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.example.defapp.databinding.LvCharactersRowBinding;

import java.util.List;

public class CharactersAdapter extends ArrayAdapter<Character> {

    public CharactersAdapter(Context context, int resource, List<Character> object){
        super(context,resource,object);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Character character = getItem(position);
        Log.w("XXXX", character.toString());

        LvCharactersRowBinding binding = null;


        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            //convertView = inflater.inflate(R.layout.lv_characters_row, parent, false);
            binding = DataBindingUtil.inflate(inflater,R.layout.lv_characters_row, parent, false);
        }else{
            binding = DataBindingUtil.getBinding(convertView);
        }

        /*
        // Unim el codi en les Views del Layout
        TextView tvCharacter = convertView.findViewById(R.id.tvCharacter);
        ImageView ivPosterImage = convertView.findViewById(R.id.ivPosterImage);
        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        tvCharacter.setText(character.getName());

        Glide.with(getContext()).load(
                character.getImageUrl()
        ).into(ivPosterImage);


        // Retornem la View replena per a mostrar-la
        return convertView;

         */

        binding.tvCharacter.setText(character.getName());
        Glide.with(getContext()).load(
                character.getImageUrl()
        ).into(binding.ivPosterImage);

        return binding.getRoot();

    }



}
