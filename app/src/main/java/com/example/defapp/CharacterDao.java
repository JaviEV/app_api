package com.example.defapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CharacterDao {
    @Query("select * from character ")
    LiveData<List<Character>> getCharacters();

    @Insert
    void addCharacter(Character character);

    @Insert
    void addCharacters(List<Character> characters);

    @Delete
    void deleteCharacter(Character character);

    @Query("DELETE FROM character")
    void deleteCharecters();

}
