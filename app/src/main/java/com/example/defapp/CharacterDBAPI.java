package com.example.defapp;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CharacterDBAPI {

    //private final String BASE_URL = "https://swapi.co/api/people/";
    private final String BASE_URL = "https://my-json-server.typicode.com/EIVII/API_characters/people";

    public ArrayList<Character> getCharacters(){
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .build();
        String url = builtUri.toString();
        return doCall(url);
    }

    private ArrayList<Character> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Character> processJson(String jsonResponse) {
        ArrayList<Character> characters = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCharacters = data.getJSONArray("results");
            for (int i = 0; i < jsonCharacters.length(); i++) {
                JSONObject jsonCharacter = jsonCharacters.getJSONObject(i);
                Character character = new Character();
                character.setName(jsonCharacter.getString("name"));
                character.setBirth_year(jsonCharacter.getString("birth_year"));
                character.setGender(jsonCharacter.getString("gender"));
                character.setHomeworld(jsonCharacter.getString("homeworld"));
                character.setImageUrl(jsonCharacter.getString("imageUrl"));


                characters.add(character);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return characters;
    }

}
