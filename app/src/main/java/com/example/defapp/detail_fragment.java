package com.example.defapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.defapp.databinding.FragmentDetailFragmentBinding;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link detail_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class detail_fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private FragmentDetailFragmentBinding binding;


    public detail_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment detail_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static detail_fragment newInstance() {
        detail_fragment fragment = new detail_fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         //view = inflater.inflate(R.layout.fragment_detail_fragment, container, false);
        binding = FragmentDetailFragmentBinding.inflate(inflater);
        view = binding.getRoot();


        Intent i = getActivity().getIntent();

        if (i != null) {
            Character character = (Character) i.getSerializableExtra("character");

            if (character != null) {
                updateUi(character);
            }
        }

        return view;
    }

    private void updateUi(Character character) {
        Log.d("CHARACTER", character.toString());

        /*
        ImageView ivPosterImage = view.findViewById(R.id.ivPosterImage);
        TextView tvName =  view.findViewById(R.id.tvName);
        TextView tvBirth_year = view.findViewById(R.id.tvBirth_year);
        TextView tvGender =   view.findViewById(R.id.tvGender);
        TextView tvHomeworld =  view.findViewById(R.id.tvHomeworld);

        tvName.setText(character.getName());
        tvBirth_year.setText("Birth year"+character.getBirth_year());
        tvGender.setText("Gender:  " + character.getGender());
        tvHomeworld.setText("Home world: "+character.getHomeworld());

        Glide.with(getContext()).load(
                character.getImageUrl()
        ).into(ivPosterImage);

         */

        binding.tvName.setText(character.getName());
        binding.tvBirthYear.setText("Birth year: "+character.getBirth_year());
        binding.tvGender.setText("Gender: "+character.getGender());
        binding.tvHomeworld.setText("Homeworld: "+character.getHomeworld());
        Glide.with(getContext()).load(
                character.getImageUrl()
        ).into(binding.ivPosterImage);

    }


}
