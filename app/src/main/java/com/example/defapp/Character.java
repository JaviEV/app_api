package com.example.defapp;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Character implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String birth_year;
    private String gender;
    private String homeworld;
    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_year() {
        return birth_year;
    }

    public void setBirth_year(String birth_year) {
        this.birth_year = birth_year;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHomeworld() {
        return homeworld;
    }

    public void setHomeworld(String homeworld) {
        this.homeworld = homeworld;
    }

    public String getImageUrl(){return imageUrl;}

    public void  setImageUrl(String imageUrl){this.imageUrl = imageUrl;}

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", birth_year='" + birth_year + '\'' +
                ", gender='" + gender + '\'' +
                ", homeworld='" +  + '\'' +
                ", imageUrl=" + imageUrl + '\''+
                '}';
    }
}
