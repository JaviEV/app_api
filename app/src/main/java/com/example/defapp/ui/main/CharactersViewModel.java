package com.example.defapp.ui.main;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.defapp.AppDatabase;
import com.example.defapp.Character;
import com.example.defapp.CharacterDBAPI;
import com.example.defapp.CharacterDao;

import java.util.ArrayList;
import java.util.List;

public class CharactersViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final CharacterDao characterDao;
    private static final int PAGES = 10;
    private LiveData<List<Character>> characters;


    public CharactersViewModel (Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.characterDao = appDatabase.getCharacterDao();

    }

    public LiveData<List<Character>>getCharacters(){
        return characterDao.getCharacters();
    }

    public void reload(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }




    public class  RefreshDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids){
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            String pais = preferences.getString("pais", "es");
            String tipusConsulta = preferences.getString(
                    "tipus_consulta", "vistes"
            );

            CharacterDBAPI api = new CharacterDBAPI();
            ArrayList<Character> result;
            result = api.getCharacters();

            characterDao.deleteCharecters();
            characterDao.addCharacters(result);

            return null;

        }
    }

}
