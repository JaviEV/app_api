package com.example.defapp.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Movie;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.defapp.Character;
import com.example.defapp.CharacterDBAPI;
import com.example.defapp.CharactersAdapter;
import com.example.defapp.DetallActivity;
import com.example.defapp.R;
import com.example.defapp.databinding.FragmentDetailFragmentBinding;

import java.util.ArrayList;
import java.util.Arrays;

import com.example.defapp.databinding.LvCharactersRowBinding;
import com.example.defapp.databinding.MainFragmentBinding;

import static com.example.defapp.databinding.FragmentDetailFragmentBinding.*;

public class MainFragment extends Fragment {

    private ArrayList<Character> items;
    private CharactersAdapter adapter;

    MainFragmentBinding binding = null;

    CharactersViewModel model ;



    private MainViewModel mViewModel;

    public MainFragment() {


    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        /*
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        ListView lvCharacters = view.findViewById(R.id.lvCharacters);


        items = new ArrayList<>();
        adapter = new CharactersAdapter(
                getContext(),
                R.layout.lv_characters_row,
                //R.id.tvCharacter,
                items
        );

        lvCharacters.setAdapter(adapter);

        lvCharacters.setOnItemClickListener((adapter, fragment, i, l) -> {
            Character character = (Character) adapter.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), DetallActivity.class);
            intent.putExtra("character", character);

            startActivity(intent);
        });

        return view;

         */


        binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();
        adapter = new CharactersAdapter(
                getContext(),
                R.layout.lv_characters_row,
                items
        );

        binding.lvCharacters.setAdapter(adapter);
        binding.lvCharacters.setOnItemClickListener((adapter, fragment, i, l) -> {
            Character character = (Character) adapter.getItemAtPosition(i);
            /*
            if (!esTablet()){
                Intent intent = new Intent(getContext(), DetallActivity.class);
                intent.putExtra("character", character);
            }else {
                sharedModel.select(character);

            }

             */
            Intent intent = new Intent(getContext(), DetallActivity.class);
            intent.putExtra("character", character);
            startActivity(intent);

        });

        model = ViewModelProviders.of(this).get(CharactersViewModel.class);
        model.getCharacters().observe(this, characters ->{
            adapter.clear();
            adapter.addAll(characters);
        });
        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_characters_fragment, menu);
    }


    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.action_refresh){
            refresh();
            Log.d("DEBUG",item.toString());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refresh() {

        RefreshDataTask task = new RefreshDataTask();
        task.execute();

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Character>> {
        @Override
        protected ArrayList<Character> doInBackground(Void... voids) {
            CharacterDBAPI api = new CharacterDBAPI();
            ArrayList<Character> result = api.getCharacters();
            Log.d("Debug", "doInBackground: " + result);
            return result;
        }
        protected void onPostExecute(ArrayList<Character> characters){
            adapter.clear();
            for (Character chara: characters ){
                adapter.add(chara);
            }
        }



    }
}
